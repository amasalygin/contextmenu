import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.2
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.1


Rectangle {
    function parentsCount(index)
    {
        var margin = 40
        if(MenuModel.parent(index) !== tree.rootIndex)
             margin += parentsCount(MenuModel.parent(index));

        return margin
    }

    function getBgColor(index, isExp, isSelected) {
        var parent = MenuModel.parent(index)

        if(parent === tree.rootIndex)
        {
            if(isSelected && !isExp) return "#3f424c";
            if(isExp)  return "#2a2b30";

            return "#3f424c";

        }
        else
        {
            if(isSelected && !MenuModel.hasChildren(index))
                return "white";

            return "#2a2b30";
        }
    }

    function getTextColor(index, isSelected) {
        var parent = MenuModel.parent(index)

        if((parent !== tree.rootIndex) && isSelected && !MenuModel.hasChildren(index))
            return "#2a2b30"

        return "#cccbcd"
    }

    function getIcon(index, isExp)
    {
        if(MenuModel.parent(index) !== tree.rootIndex)
        {
            if(isExp) return "icons/dropped.png"
            else      return "icons/hidden.png"
        }
        else
            return MenuModel.data(index, 1) // 1 - Icon role
    }


    Rectangle{
        x: 8
        y: 8
        width: 100
        height: 40
        color: "#cccbcd"
        MouseArea {
            id: mouseArea
            anchors.fill: parent
            acceptedButtons: Qt.RightButton


            Text {
                id: text1
                text: qsTr("Button")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                font.pixelSize: 12
                renderType: Text.NativeRendering
                color: "black"
            }

            Menu {
                id: menu
                y: mouseArea.height
                width: 300
                height: 700

                background: Rectangle{
                    color: "#3f424c"
                }

                contentItem:TreeView{
                    id: tree
                    alternatingRowColors: false
                    backgroundVisible: false
                    model: MenuModel

                    onClicked: {
                        isExpanded(index) ? collapse(index) : expand(index)
                    }

                    TableViewColumn {
                        role: "name"
                    }

                    headerDelegate: Rectangle{
                        height: 70
                        color: "#3f424c"

                        Image {
                            anchors.right: parent.right
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            anchors.margins: 20
                            width: 24
                            height: 24
                            fillMode: Image.PreserveAspectFit
                            source: "icons/settings.png"
                        }
                        Rectangle{
                            anchors.top: parent.bottom
                            width: parent.width
                            height: 1
                            color: "#2a2b30"
                        }
                    }

                    style: TreeViewStyle{
                        id: treeStyle
                        frame: Rectangle{color: "#3f424c"; border.width: 0}
                        branchDelegate: Rectangle{}
                        indentation: 0

                        rowDelegate: Rectangle{
                            height: 60
                        }

                        itemDelegate: Item{
                            id: _itemDelegate
                            property var _color: "black"
                            Rectangle{
                                id: _itemDelegateRect
                                anchors.fill: parent
                                color: getBgColor(styleData.index, styleData.isExpanded, styleData.selected)

                                Image {
                                    id: itemIcon
                                    anchors.leftMargin: MenuModel.parent(styleData.index) !== tree.rootIndex ? parentsCount(styleData.index) : 10
                                    anchors.left: parent.left
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    width: MenuModel.parent(styleData.index) !== tree.rootIndex ? 16 : 26
                                    height: MenuModel.parent(styleData.index) !== tree.rootIndex ? 16 : 26
                                    fillMode: Image.PreserveAspectFit
                                    source: getIcon(styleData.index, styleData.isExpanded)
                                }

                                Text{
                                    id: menuText
                                    anchors.left: itemIcon.right
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.leftMargin: 15
                                    verticalAlignment: Text.AlignVCenter
                                    text: styleData.value
                                    color: getTextColor(styleData.index, styleData.selected)
                                    elide: Text.ElideRight
                                    renderType: Text.NativeRendering

                                    font.family: "MS Reference Sans Serif"
                                    horizontalAlignment: Text.AlignHCenter
                                    font.pixelSize: MenuModel.parent(styleData.index) !== tree.rootIndex ? 16 : 22

                                }

                                Image {
                                    id: itemRightIcon
                                    anchors.leftMargin: 10
                                    anchors.left: menuText.right
                                    anchors.top: parent.top
                                    anchors.bottom: parent.bottom
                                    visible: (( MenuModel.parent(styleData.index) === tree.rootIndex ) && ( MenuModel.hasChildren(styleData.index)) ) ? true : false
                                    width: 16
                                    height: 16
                                    fillMode: Image.PreserveAspectFit
                                    source: styleData.isExpanded ? "icons/up.png" :  "icons/dropped.png"
                                }
                            }
                        }
                    }
                }
            }

        }
    }





    Connections {
        target: mouseArea
        onClicked: {
            menu.open()
        }
    }

}
