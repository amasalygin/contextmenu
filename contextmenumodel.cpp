#include "contextmenumodel.h"
#include "QDebug"

ContextMenuModel::ContextMenuModel(QObject *parent)
    : QAbstractItemModel(parent)
{
    QString str = "Settings";
    _rootItem = new MenuItem(str);
}

ContextMenuModel::~ContextMenuModel()
{
    delete _rootItem;
}


QHash<int, QByteArray> ContextMenuModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameEnum] = "name";
    return roles;
}

void ContextMenuModel::appendItem(MenuItem *item)
{
    _rootItem->appendChild(item);
}

MenuItem *ContextMenuModel::rootItem()
{
    return _rootItem;
}


int ContextMenuModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<MenuItem*>(parent.internalPointer())->columnCount();
    else
        return _rootItem->columnCount();
}

QVariant ContextMenuModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    MenuItem *item = static_cast<MenuItem*>(index.internalPointer());

    switch (role) {
    case NameEnum:
        return item->data(NameEnum);

    case IconEnum:
        return item->data(IconEnum);
    default:
        return QVariant();
    }
}

Qt::ItemFlags ContextMenuModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant ContextMenuModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return _rootItem->data(section);

    return QVariant();
}

QModelIndex ContextMenuModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    MenuItem *parentItem;

    if (!parent.isValid())
        parentItem = _rootItem;
    else
        parentItem = static_cast<MenuItem*>(parent.internalPointer());

    MenuItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex ContextMenuModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    MenuItem *childItem = static_cast<MenuItem*>(index.internalPointer());
    MenuItem *parentItem = childItem->parentItem();

    if (parentItem == _rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int ContextMenuModel::rowCount(const QModelIndex &parent) const
{
    MenuItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = _rootItem;
    else
        parentItem = static_cast<MenuItem*>(parent.internalPointer());

    return parentItem->childCount();
}

