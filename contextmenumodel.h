#ifndef CONTEXTMENUMODEL_H
#define CONTEXTMENUMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include "menuitem.h"



class ContextMenuModel : public QAbstractItemModel
{
    Q_OBJECT
    enum DelegateEnum
    {
        NameEnum = 0,
        IconEnum = 1
    };

public:
    explicit ContextMenuModel(QObject *parent = 0);
    ~ContextMenuModel();

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QHash<int, QByteArray> roleNames() const;

    void appendItem(MenuItem* item);
    MenuItem *rootItem();
private:
    MenuItem *_rootItem;
};
//! [0]

#endif // CONTEXTMENUMODEL_H
