#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "contextmenumodel.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    ContextMenuModel *model = new ContextMenuModel();

    MenuItem *Status = new MenuItem("Status", "icons/status.png" ,model->rootItem());

    MenuItem *Category = new MenuItem("Category", "icons/categories.png" ,model->rootItem());
    Category->appendChild(new MenuItem("Appetizer", "" ,Category));
    Category->appendChild(new MenuItem("Entree", "" ,Category));
    Category->appendChild(new MenuItem("Drinks", "" ,Category));
    Category->appendChild(new MenuItem("Desert", "" ,Category));

    MenuItem *CardColor = new MenuItem("Card Color","icons/cards.png" ,model->rootItem());
        CardColor->appendChild(new MenuItem("Green", "" ,CardColor));
        CardColor->appendChild(new MenuItem("Yellowwwwwwwwwwwwwwwwwwwwwwwwwwwwww","" ,CardColor));
            MenuItem *Red = new MenuItem("Red", "" ,CardColor);
            MenuItem *Third = new MenuItem("Third level",  "" ,Red);
                Third->appendChild(new MenuItem("fourth_level", "" ,Third));
            Red->appendChild(Third);
       CardColor->appendChild(Red);

    model->appendItem(Status);
    model->appendItem(Category);
    model->appendItem(CardColor);

    engine.rootContext()->setContextProperty("MenuModel", model);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));


    return app.exec();
}
