#include "menuitem.h"
#include "QStringList"

MenuItem::MenuItem(const QString &data, const QString &iconPath, MenuItem *parent)
{
    m_parentItem = parent;
    m_itemData << data;
    if(!iconPath.isEmpty())
        m_itemData << iconPath;


}

MenuItem::~MenuItem()
{
    qDeleteAll(m_childItems);
}

void MenuItem::appendChild(MenuItem* item)
{
    m_childItems.append(item);
}

MenuItem *MenuItem::child(int row)
{
    return m_childItems.value(row);
}

int MenuItem::childCount() const
{
    return m_childItems.count();
}

int MenuItem::columnCount() const
{
    return m_itemData.count();
}

QVariant MenuItem::data(int column) const
{
    return m_itemData.value(column);
}

MenuItem *MenuItem::parentItem()
{
    return m_parentItem;
}

int MenuItem::row() const
{
    if (m_parentItem)
        return m_parentItem->m_childItems.indexOf(const_cast<MenuItem*>(this));

    return 0;
}
