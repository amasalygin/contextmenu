#ifndef MENUITEM_H
#define MENUITEM_H


#include <QList>
#include <QVariant>
#include "QBrush"


class MenuItem
{

public:
    explicit MenuItem(const QString &data, const QString &iconPath = 0, MenuItem *parentItem = 0);
        ~MenuItem();

        void appendChild(MenuItem *item);

        MenuItem *child(int row);
        int childCount() const;
        int columnCount() const;
        QVariant data(int column) const;
        int row() const;
        MenuItem *parentItem();

    private:
        QList<MenuItem*> m_childItems;
        QList<QVariant> m_itemData;
        MenuItem *m_parentItem;
};

#endif // MENUITEM_H
